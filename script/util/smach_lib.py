#!/usr/bin/env python

import rospy
import smach
import smach_ros
import time
import os
import rospkg
import playsound
from std_srvs.srv import Empty
from std_msgs.msg import String
from skuba_nav_lib.navigation import Navigation
from skuba_athome_msgs.srv import SpeakCommand,NoiseAdjustCommand,SpeechCommand
from dynamixel_workbench_msgs.srv import DynamixelCommand
from util.move_arm import *


class Prepared(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['finish'])
    
    def execute(self,userdata):
        rospy.wait_for_service("/speech/speak_thai",)
        print("/speech/speak_thai")
        rospy.wait_for_service("/speech/noise_adjust")
        print("/speech/noise_adjust")
        rospy.wait_for_service("/speech/listen")
        print("/speech/listen")
        rospy.wait_for_service("/dynamixel_workbench/dynamixel_command")
        print("/dynamixel_workbench/dynamixel_command")

        rospy.logwarn('Executing STATE : Prepared ')
        return 'finish'

class ListenToStart(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['Say_Hi','lead_me'],)
        self.noise_adjust = rospy.ServiceProxy('/speech/noise_adjust', NoiseAdjustCommand)
        self.listen = rospy.ServiceProxy('/speech/listen', SpeechCommand)

    def execute(self,userdata):
        self.word = ''
        self.noise_adjust(True)
        rospy.sleep(1)
        print("Say something after beep sound")
        rospack = rospkg.RosPack()
        playsound.playsound( rospack.get_path('skuba_athome_speechprocessing')+"/beep.mp3",True)

        while not rospy.is_shutdown():
            self.lis_word = self.listen(True)
            self.word = str(self.lis_word.sentence)

            if self.word  == "สวัสดี":
                print(self.word)
                rospy.logwarn('Executing STATE : Listen To Start for สวัสดี angus')
                return 'Say_Hi'
            elif self.word  == "นำทางฉัน":
                print(self.word)
                rospy.logwarn('Executing STATE : Listen To Start for นำทางฉัน')
                return 'lead_me'
        

class ListenNoTimeout(smach.State):
    def __init__(self,sentence):
        smach.State.__init__(self, outcomes=['finish'])
        self.sentence = sentence
        self.noise_adjust = rospy.ServiceProxy('/speech/noise_adjust', NoiseAdjustCommand)
        self.listen = rospy.ServiceProxy('/speech/listen', SpeechCommand)

    def execute(self,userdata):
        self.word = ''
        self.noise_adjust(True)
        rospy.sleep(1)
        print("Say something after beep sound")
        rospack = rospkg.RosPack()
        playsound.playsound( rospack.get_path('skuba_athome_speechprocessing')+"/beep.mp3",True)

        while not rospy.is_shutdown():
            self.lis_word = self.listen(True)
            self.word = str(self.lis_word.sentence)

            if self.word  == self.sentence:
                print(self.word)
                break
        rospy.logwarn('Executing STATE : Listen No Timeout for ' + self.sentence)
        return 'finish'


class Talk(smach.State):
    def __init__(self, sentence):
        smach.State.__init__(self, outcomes=['finish'])
        self.sentence = sentence
        self.speak = rospy.ServiceProxy("/speech/speak_thai",SpeakCommand)

    def execute(self,userdata):
        while(self.speak(self.sentence)==None):
            pass
        rospy.logwarn('Executing STATE : Talk '+self.sentence)
        return 'finish'


class ListenForLocation(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['finish',"timeout","no_location"], output_keys=['location'])
        self.noise_adjust = rospy.ServiceProxy('/speech/noise_adjust', NoiseAdjustCommand)
        self.listen = rospy.ServiceProxy('/speech/listen', SpeechCommand)

    def execute(self, userdata):
        self.noise_adjust()
        rospy.sleep(1)
        print("Say something after beep sound")
        rospack = rospkg.RosPack()
        fututre = time.time() + 10
        playsound.playsound( rospack.get_path('skuba_athome_speechprocessing')+"/beep.mp3",True)

        while not rospy.is_shutdown():
            self.lis_word = self.listen(True)
            self.word = str(self.lis_word.sentence)

            if self.word != '' and self.word == "บ้าน" or self.word == "ร้านค้า":
                self.location = self.word
                break
            elif self.word != '' and self.word == "nothing":
                return 'no_location'
            if time.time() > fututre:
                rospy.logwarn('Executing STATE : ListenForLocation Timeout')
                return 'timeout'
        userdata.location = self.location
        rospy.logwarn('Executing STATE : ListenForLocation ' + self.location)
        return 'finish'


class TalkLocation(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['finish'], input_keys=['location'])
        self.speak = rospy.ServiceProxy("/speech/speak_thai",SpeakCommand)
    
    def execute(self,userdata):
        self.sentence = ''
        self.sentence = ",,คุณต้องการไปที่ " + str(userdata.location) + "ใช่หรือไม่"
        while(self.speak(self.sentence)==None):
            pass
        rospy.logwarn('Executing STATE : Talk '+self.sentence)
        return 'finish'


class RecheckLocation(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=["correct","incorrect","timeout"], input_keys=["location"])
        self.noise_adjust = rospy.ServiceProxy('/speech/noise_adjust', NoiseAdjustCommand)
        self.listen = rospy.ServiceProxy('/speech/listen', SpeechCommand)
    
    def execute(self, userdata):
        self.noise_adjust()
        rospy.sleep(1)
        print("Say something after beep sound")
        rospack = rospkg.RosPack()
        fututre = time.time() + 10
        playsound.playsound( rospack.get_path('skuba_athome_speechprocessing')+"/beep.mp3",True)

        self.sentence = userdata.location
        while not rospy.is_shutdown():
            self.lis_word = self.listen(True)
            self.word = str(self.lis_word.sentence)
            if self.word == "ใช่":
                rospy.logwarn('Executing STATE : RecheckLocation '+self.sentence)
                return "correct"
            elif self.word == "ไม่":
                rospy.logwarn('Executing STATE : RecheckLocation '+self.sentence)
                return "incorrect"
            if time.time() > fututre:
                rospy.logwarn('Executing STATE : RecheckLocation Timeout')
                return 'timeout'


class TalkgoingtoLocation(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['finish'], input_keys=['location'])
        self.speak = rospy.ServiceProxy("/speech/speak_thai",SpeakCommand)
    
    def execute(self,userdata):
        self.sentence = ''
        self.sentence = ",,ฉันกำลังจะพาคุณไปที่ " + str(userdata.location) 
        while(self.speak(self.sentence)==None):
            pass
        rospy.logwarn('Executing STATE : Talk '+self.sentence)
        return 'finish'


class Nav(smach.State):

    '''
    def __init__(self):
        smach.State.__init__(self, outcomes=['finish'], input_keys=['location'])
        pass

    def execute(self,userdata):
        self.location = str(userdata.location)
        print("Went to "+ self.location)
        rospy.logwarn("Executing STATE : go to "+self.location)
        return 'finish'

    '''
    def __init__(self):
        smach.State.__init__(self, outcomes=['finish'], input_keys=['location'])
        self.nav = Navigation("/home/angus/skuba_ws/src/skuba_show_smach/script/util/file.csv")
        self.clear_map = rospy.ServiceProxy("/move_base/clear_costmaps",Empty)

    def execute(self,userdata):
        self.location = str(userdata.location)
        if self.location == "บ้าน":
            self.nav.go_to_position("third_pos")
            self.clear_map()
            if(self.nav.get_state(120)):
                  pass
            else:
                self.clear_map()
                self.nav.go_to_position("third_pos")

        elif self.location == "ร้านค้า":
            self.nav.go_to_position("second_pos")
            self.clear_map()
            if(self.nav.get_state(120)):
                pass
            else:
                self.clear_map()
                self.nav.go_to_position("second_pos")

        rospy.logwarn("Executing STATE : go to "+self.location)
        return 'finish'


class Talk_arrive_at_location(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['finish'], input_keys=['location'])
        self.speak = rospy.ServiceProxy("/speech/speak_thai",SpeakCommand)
    
    def execute(self,userdata):
        self.sentence = ''
        self.sentence = "ฉันได้พาคุณมาถึง" + str(userdata.location) + "แล้ว"
        while(self.speak(self.sentence)==None):
            pass
        rospy.logwarn('Executing STATE : Talk '+self.sentence)
        return 'finish'


class GotoHome(smach.State):


    '''
    def __init__(self):
        smach.State.__init__(self, outcomes=['finish'])

    def execute(self,userdata):
        print("Went to ")
        rospy.logwarn("Executing STATE : go to ")
        return 'finish'
    '''

    def __init__(self):
        smach.State.__init__(self, outcomes=['finish'])
        self.nav = Navigation("/home/angus/skuba_ws/src/skuba_show_smach/script/util/file.csv")
        self.clear_map = rospy.ServiceProxy("/move_base/clear_costmaps",Empty)

    def execute(self,userdata):
        self.nav.go_to_position("home")
        self.clear_map()
        if(self.nav.get_state(110)):
                pass
        else:
            self.clear_map()
            self.nav.go_to_position("home")

        rospy.logwarn("Executing STATE : go to home")
        return 'finish'


class move_arm(smach.State):

    def __init__(self):
        smach.State.__init__(self, outcomes=['finish'])

    def execute(self,userdata):

        print("wait_move")
        # rospy.sleep(7)
        # print("move arm")
        
        rospy.wait_for_service("/dynamixel_workbench/dynamixel_command")
        move_arm = rospy.ServiceProxy("/dynamixel_workbench/dynamixel_command",DynamixelCommand)
        move_arm("",0,"Goal_Position",2048)
        move_arm("",1,"Goal_Position",2408)
        move_arm("",2,"Goal_Position",2492)
        rospy.sleep(2)
        move_arm("",0,"Goal_Position",1807)
        rospy.sleep(0.5)
        move_arm("",0,"Goal_Position",2048)
        rospy.sleep(0.5)
        move_arm("",0,"Goal_Position",1807)
        rospy.sleep(0.5)
        move_arm("",0,"Goal_Position",2048)
        rospy.sleep(0.5)    
        move_arm("",0,"Goal_Position",1807)
        move_arm("",1,"Goal_Position",3254)
        move_arm("",2,"Goal_Position",1694)


        rospy.logwarn("Executing STATE : move_arm")
        return 'finish'


class speak_hi(smach.State):

    def __init__(self):
        smach.State.__init__(self, outcomes=['finish'])
        self.sentence = "สวัสดี, ฉันชื่อ, Angus"
        self.speak = rospy.ServiceProxy("/speech/speak_thai",SpeakCommand)

    def execute(self,userdata):
        print("wait_speak")
        rospy.sleep(2)
        while(self.speak(self.sentence)==None):
            pass
        rospy.logwarn('Executing STATE : Talk '+self.sentence)
        return 'finish'